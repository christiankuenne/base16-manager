# base16-manager

My scripts to automate [base16](https://github.com/chriskempson/base16) tasks.

## Installation

```
make install
```

This will clone the required projects [InspectorMustache/base16-builder-python](https://github.com/InspectorMustache/base16-builder-python) and [themix-project/oomox-gtk-theme](https://github.com/themix-project/oomox-gtk-theme).

## Usage

You need one repository with a list of color schemes and one repository with a list of templates. The repositories are specified in [sources.yaml](./sources.yaml).

### Update color schemes and templates

```
make update
```

This will create a subfolder `sources` which contains a list of all color schemes and a list of all templates. Afterwards the `schemes` and `templates` are downloaded.

### Build all themes

```
make build
```

This will create a subfolder `output` with all generated themes.

### Apply a theme

```
theme-selector [THEME]
```

A selection will be shown if the theme is not specified.

### Clean

```
make clean
```

This removes sources, color schemes, templates and generated themes.

## Themes

The following themes are applied:

* GTK Theme