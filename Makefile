install:
	git clone https://github.com/InspectorMustache/base16-builder-python
	git clone https://github.com/themix-project/oomox-gtk-theme

uninstall:
	rm -rf base16-builder-python
	rm -rf oomox-gtk-theme

update:
	base16-builder-python/pybase16.py update -c

build:
	base16-builder-python/pybase16.py build

clean:
	rm -rf output
	rm -rf schemes
	rm -rf sources
	rm -rf templates